<?php
namespace TeamRadHq\Script\Traits;
/**
 * This trait contains methods for applying colour 
 * formatting to terminal output.
 */
trait ColorSchemeTraits {
	/**
	 * A class helper for outputting colored text to the terminal.
	 * @var object TeamRadHq\Script\Traits\Colors
	 */
	protected $colors;

	/**
	 * An array of color schemes which are used by 
	 * TeamRadHq\Script\Colors to output text.
	 * @var array
	 */
	protected $colorSchemes = array(
		"gray" => ["fg_color" => "black", "bg_color" => "light_gray"],
		"black" => ["fg_color" => "light_gray", "bg_color" => "black"],
		"red" => ["fg_color" => "white", "bg_color" => "red"],
		"green" => ["fg_color" => "white", "bg_color" => "green"],
		"blue" => ["fg_color" => "white", "bg_color" => "blue"],
		"yellow" => ["fg_color" => "black", "bg_color" => "yellow"],
		"purple" => ["fg_color" => "white", "bg_color" => "magenta"],
		"darkGray" => ["fg_color" => "light_gray", "bg_color" => "black"],
		"darkRed" => ["fg_color" => "red", "bg_color" => "black"],
		"darkGreen" => ["fg_color" => "green", "bg_color" => "black"],
		"darkBlue" => ["fg_color" => "blue", "bg_color" => "black"],
		"darkYellow" => ["fg_color" => "yellow", "bg_color" => "black"],
		"darkPurple" => ["fg_color" => "purple", "bg_color" => "black"],
		);

	/**
	 * Loads the instance of TeamRadHq/Script/Colors.
	 */
	protected function loadColors() {
		if( empty($this->colors) )
			$this->colors = new \TeamRadHq\Script\Traits\Colors;
	}

	/**
	 * Returns a color scheme's background color.
	 * @param  string $colorScheme The color scheme.
	 * @return string              The scheme's background colour.
	 */
	protected function bg($colorScheme=null) {
		if( $this->isColorScheme($colorScheme) ) 
			return $this->colorSchemes[$colorScheme]["bg_color"];
	}

	/**
	 * Returns a color scheme's foreground color.
	 * @param  string $colorScheme The color scheme.
	 * @return string              The scheme's foreground colour.
	 */
	protected function fg($colorScheme=null) {
		if( $this->isColorScheme($colorScheme) ) 
			return $this->colorSchemes[$colorScheme]["fg_color"];
	}
	
	/**
	 * Returns true if $colorScheme exists.
	 * @param  string  $colorScheme The name of the color scheme to check.
	 * @return boolean              True if $colorScheme exists.
	 */
	protected function isColorScheme($colorScheme) {
		if( in_array($colorScheme, array_keys($this->colorSchemes)) ) 
			return true;
	}
}