<?php
namespace TeamRadHq\Script\Traits;

/**
 * ArgumentTraits contains methods for parsing flags
 * and arguments passed to a script from the command 
 * line. 
 */
trait ArgumentTraits {
	
	/**
	 * An array of allowed flags for this 
	 * script. If there are no allowed flags, 
	 * none can be passed to the script.
	 * @var array
	 */
	protected $flags = array();

	/**
	 * An array of allowed options for this 
	 * script. If there are no allowed options,  
	 * none can be passed to the script.
	 * @var array
	 */
	protected $options = array();

	/**
	 * calls setFlags and setOptions.
	 */
	public function setArguments() {
		$this->setFlags();
		$this->setOptions();
	}

	/**
	 * Sets the allowedFlags for the script.
	 * @param array $flags 	An array of allowed flags
	 *                      prepended with '-'.
	 */
	public function setAllowedFlags(array $flags) {
		$this->allowedFlags = $flags;
	}

	/**
	 * Sets the allowedFlags for the script.
	 * @param array $flags 	An array of allowed options.
	 */
	public function setAllowedOptions(array $options) {
		$this->allowedOptions = $options;
	}

	/**
	 * Returns true if $flag is set.
	 * @param  string $flag The flag you want to check.
	 * @return boolen       True if the flag is set.
	 */
	public function flag($flag) {
		if( in_array($flags, $this->flags) )
			return true;
	}
	
	/**
	 * Returns an option's value if it is set.
	 * @param  string $option The option whose value you want to retrieve.
	 * @return string         The option's value.
	 */
	public function option($option) {
		if( in_array($options, array_keys($this->options)) )
			return $this->options[$option];
	}

	/**
	 * Gets the global argv and parses its contents into 
	 * the flags array.
	 * @param array $flags An array of flags to set.
	 */
	protected function setFlags() {
		if( empty($this->allowedFlags) ) return;
		// Only set flags if they are allowed.
		$this->flags = array_intersect($_SERVER["argv"], $this->allowedFlags);
	}

	/**
	 * Sets all options from allowedOptions
	 * @param array $options An array of options to set.
	 */
	protected function setOptions() {
		if( empty($this->allowedOptions) ) return;
		$this->options  = getopt(implode(':',$this->allowedOptions).":");
	}

}
?>