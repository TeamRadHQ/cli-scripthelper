<?php
namespace TeamRadHq\Script\Traits;
class Colors {

	/**
	 * An array of foreground colors and 
	 * their numeric references.
	 * @var array
	 */
	private $foreground_colors = array(
		'black' => '0;30',
		'dark_gray' => '1;30',
		'blue' => '0;34',
		'light_blue' => '1;34',
		'green' => '0;32',
		'light_green' => '1;32',
		'cyan' => '0;36',
		'light_cyan' => '1;36',
		'red' => '0;31',
		'light_red' => '1;31',
		'purple' => '0;35',
		'light_purple' => '1;35',
		'brown' => '0;33',
		'yellow' => '1;33',
		'light_gray' => '0;37',
		'white' => '1;37'
	);

	/**
	 * An array of background colours and 
	 * their numeric references.
	 * @var array
	 */
	private $background_colors = array(
		'black' => '40',
		'red' => '41',
		'green' => '42',
		'yellow' => '43',
		'blue' => '44',
		'magenta' => '45',
		'cyan' => '46',
		'light_gray' => '47',
	);
	

	/**
	 * Gets a $string and formats gives applies $fg_color and $bg_color.
	 * @param  string $string   The string you want to format.
	 * @param  string $fg_color The text foreground color.
	 * @param  string $bg_color The text background color.
	 * @return string           A color formatted $string.
	 */
	public function getColoredString($string, $fg_color = null, $bg_color = null) {
		$colored_string = "";	
		// Check if given foreground color found
		if ( isset( $this->foreground_colors[$fg_color] ) ) {
			$colored_string .= "\033[" . $this->foreground_colors[$fg_color] . "m";
		}

		// Check if given background color found
		if ( isset( $this->background_colors[$bg_color] ) ) {
			$colored_string .= "\033[" . $this->background_colors[$bg_color] . "m";
		}

		// Add string and end coloring
		$colored_string .=  $string . "\033[0m";
		
		return $colored_string;
	}
	

	/**
	 * Gets a $string and formats echoes it using $fg_color and $bg_color.
	 * @param  string $string   The string you want to format.
	 * @param  string $fg_color The text foreground color.
	 * @param  string $bg_color The text background color.
	 * @return string           A color formatted $string.
	 */
	public function _echo($string, $fg_color = null, $bg_color = null) {
		echo $this->getColoredString($string, $fg_color, $bg_color);
	}


	/**
	 * Returns the names of all foreground colour names.
	 * @return array An array of foreground colour names.
	 */
	public function getForegroundColors() {
		return array_keys($this->foreground_colors);
	}
	

	/**
	 * Returns the names of all background colour names.
	 * @return array An array of background colour names.
	 */
	public function getBackgroundColors() {
		return array_keys($this->background_colors);
	}

	/**
	 * A test function which outputs examples of all 
	 * background and foreground colours. 
	 * @return string Terminal output.
	 */
	public function display() {
		echo "\n :::::::::Foreground:::::::\n";
		foreach($this->getForegroundColors() as $fg_color) {
			$this->_echo("$fg_color\n", $fg_color);
		}
		echo "\n :::::::::Background:::::::\n";
		foreach($this->getBackgroundColors() as $bg_color ) {
			$this->_echo("$bg_color \n", 'white', $bg_color );
		}
		return;
	}

}

?>