<?php
namespace TeamRadHq\Script\Traits;
/**
 * This trait contains properties and methods for outputting text
 * to the terminal user. It contains tools for formatting terminal
 * output. 
 * @todo  Consider deprecating block() and inline().
 * @todo  Look at center and figure out why even 
 *        numbered strings are displayed crooked.
 * @todo  Look at line() and determine how to handle new lines. 
 * @todo  Look at clear(). Consider clearTerminal() or something...
 */
trait OutputTraits {
	use ColorSchemeTraits;	
	/**
	 * Buffer for terminal output.
	 * @var string
	 */
	protected $outputBuffer;

	/**
	 * Outputs then clears the terminal buffer.
	 * @return $this 
	 */
	public function out(){
		echo $this->outputBuffer;
		unset($this->outputBuffer);
		return $this;
	}

	/**
	 * Formats $string to $colorScheme and then outputs to terminal buffer.
	 * @param  string $string      The string to output.
	 * @param  string $colorScheme The color scheme to use.
	 * @return string $this->output The terminal buffer.
	 */
	protected function output($string, $colorScheme=null) {
		// Set background and foreground to null or colors from scheme.
		$this->outputBuffer.= $this->colors->getColoredString($string, $this->fg($colorScheme), $this->bg($colorScheme));
		return $this;
	}

	/**
	 * Echoes an black escaped new line. 
	 * @return string A newline string which resets terminal colours.
	 */
	public function n() {
		$this->output("\n",'light_gray','black');
		return $this;
	}

	/**
	 * Outputs a tab escape character.
	 * @return string A tab escape.
	 */
	public function t() {
		$this->output("\t");
		return $this;
	}

	/**
	 * Outputs left aligned text to terminal with $colorScheme and $width.
	 * @param  string  $string      The string you want to output.
	 * @param  string  $colorScheme The color scheme you want to use.
	 * @param  integer $width       The screen width.
	 * @return string               Outputs $string to the terminal.
	 */
	public function left($string, $colorScheme=null, $width=80) {
		$this->block($string, $colorScheme, $width);
		return $this;
	}

	/**
	 * Outputs center aligned text to terminal with $colorScheme and $width.
	 * @param  string  $string      The string you want to output.
	 * @param  string  $colorScheme The color scheme you want to use.
	 * @param  integer $width       The screen width.
	 * @return string               Outputs $string to the terminal.
	 */
	public function center($string, $colorScheme=null, $width=80) {
		$this->block($string, $colorScheme, $width, STR_PAD_BOTH);
		return $this;
	}

	/**
	 * Outputs center right text to terminal with $colorScheme and $width.
	 * @param  string  $string      The string you want to output.
	 * @param  string  $colorScheme The color scheme you want to use.
	 * @param  integer $width       The screen width.
	 * @return string               Outputs $string to the terminal.
	 */
	public function right($string, $colorScheme=null, $width=80) {
		$this->block($string, $colorScheme, $width, STR_PAD_LEFT);
		return $this;
	}

	/**
	 * Outputs inline text to terminal with $colorScheme and $width.
	 * @param  string  $string      The string you want to output.
	 * @param  string  $colorScheme The color scheme you want to use.
	 * @return string               Output $string.
	 */
	public function inline($string, $colorScheme=null) {
		$this->output($string, $colorScheme);
		return $this;
	}

	/**
	 * Outputs block formatted $string to the terminal.
	 * @param  string $string      	The string you want to output.
	 * @param  string $colorScheme 	The color scheme you want to use.
	 * @param  integer $width  		The screen width.
	 * @param  string $STR_PAD   	STR_PAD_RIGHT, STR_PAD_BOTH, STR_PAD_LEFT or other valid filters. 
	 * @return string 	Outputs formatted string to terminal.
	 */
	protected function block($string, $colorScheme=null, $width=80, $STR_PAD=STR_PAD_RIGHT) {
		// Pad the $string using $STR_PAD filter.
		$string = $this->str_pad($string, $width, $STR_PAD);

		// Echo colored text or not.
		$this->output($string, $colorScheme);
		return $this;
	}
	
	/**
	 * Clears the terminal window.
	 * @return string A clear screen!
	 */
	public function clear() {
		system('clear');
		return $this;
	}

	/**
	 * Pads $string by $width using supplied $STR_PAD filter.
	 * @param  string  $string 	The string you want to pad.
	 * @param  integer $width  	The screen width you want to align to.
	 * @param  FILTER $width  	The STR_PAD filter you want to use for padding.
	 * @return string          	A padded $string.
	 */
	protected function str_pad($string, $width=80, $STR_PAD=STR_PAD_RIGHT) {
		// Pad $string if $width is longer, or return $string as is. 
		if ($width > strlen($string)) {
			$string = str_pad($string, $width - strlen($string/2), ' ', $STR_PAD);
			$string = $this->makeWidth($string, $width);
		}
		return $string;
	}

	/**
	 * Formats $string so that it's $width long. If the string
	 * is shorter, it is padded with whitespace, if it's longer, 
	 * it's truncated at $width.
	 * @param  string  $string The string whose width you want to set.
	 * @param  integer $width  The width you want ot set $string to.
	 * @return string          A string that is $width characters long.
	 */
	protected function makeWidth($string, $width=80) {
		$strlen = strlen($string); // Get lenght
		if ($strlen < $width) {
			$string = str_pad($string, $width, ' ');
		} else {
			$string = substr($string, 0, $width);
		}
		return $string;
	}

	/**
	 * Output a line of $string that is $width long and uses $colorScheme
	 * @param  string  $string      The character to make the line.
	 * @param  string  $colorScheme The colour scheme you want to apply.
	 * @param  integer $width       The width of the line.
	 * @return string 	Outputs a line of $string characters $width long
	 */
	public function line($separator='-', $colorScheme=null,$width=80) {
		// Repeat $separator, truncate at $width, then output with newline.
		$line = str_repeat($separator, $width);
		$line = $this->makeWidth($line, $width, $separator);
		$this->output($line, $colorScheme)->n();
		return $this;
	}
	public function error($message) {
		$this->line()->inline('Error:', 'red')->inline(' '.$message)->n()->line();
	}

}
?>