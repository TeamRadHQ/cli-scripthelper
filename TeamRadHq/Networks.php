#!/usr/bin/php
<?php
require_once('autoload.php');
class Networks extends TeamRadHq\Script\Script {
	/**
	 * A list of visible wireless networks.
	 * @var array
	 */
	protected $networkList = array();
	/**
	 * Lists disallowed ID's for string to array 
	 * conversion of network scan results.
	 * @var array
	 */
	protected $disallowedIDs =  array('#','');
	/**
	 * Lists disallowed SSID's for string to array 
	 * conversion of network scan results.
	 * @var array
	 */
	protected $disallowedSSIDs =  array('', '<hidden>');
	/**
	 * A list of known network SSID's. If a network 
	 * is known, it means that there is sufficient
	 * information to connect to it.
	 * @var array
	 */
	protected $knownNetworks = array('iggyPhone', 'Gwondanaland');
	/**
	 * The command to execute when performing a scan.
	 * @var string
	 */
	protected $scanNetworks = 'wicd-cli --wireless --scan --list-networks';
	public function connect(){}

	public function disconnect(){}

	public function list(){
		return $this->networkList;
	}

	public function listKnown(){}

	public function save($SSID, $password=null){}

	public function update($SSID,$password=null){}

	public function delete($SSID){}

	public function scan(){
		$this->setNetworkList(shell_exec($this->scanNetworks));
	}

	protected function setNetworkList($output) {
		$this->networkList = array();
		$rows = explode("\t", $output);
		foreach ($rows as $row) {
			$this->networkList[] = $this->makeNet($row[0], $row[3], $row[1]);
		}
	}
	protected function makeNet($id, $ESSID, $BSSID) {
		return array(
			"id" => $id,
			"ESSID" => $ESSID
			"BSSID" => $BSSID,
		);
	}


} // end Class

$n = new Networks;
?>