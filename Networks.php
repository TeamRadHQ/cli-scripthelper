#!/usr/bin/php
<?php
require_once('autoload.php');
class Networks extends TeamRadHq\Script\Script {
	/**
	 * A list of visible wireless networks.
	 * @var array
	 */
	protected $networkList = array();
	/**
	 * Lists disallowed SSID's for string to array 
	 * conversion of network scan results.
	 * @var array
	 */
	protected $disallowedBSSIDs =  array('', '<hidden>', 'BSSID');
	/**
	 * A list of known network SSID's. If a network 
	 * is known, it means that there is sufficient
	 * information to connect to it.
	 * @var array
	 */
	protected $knownNetworks = array('iggyPhone', 'Gwondanaland');
	/**
	 * The command to execute when performing a scan.
	 * @var string
	 */
	protected $scanNetworks = 'wicd-cli --wireless --scan --list-networks';
	public function connect(){}
	public function disconnect(){}
	/**
	 * Returns a list of the last network scan results.
	 * @return array An array of found networks.
	 */
	public function listNetworks(){
		return $this->networkList;
	}
	/**
	 * Returns a list of the known networks found during 
	 * the last network scan.
	 * @return array An array of found known networks.
	 */
	public function listKnown(){
		$knownNetworks = array();
		foreach ($this->networkList as $net) {
			if ( in_array($net["ESSID"], $this->knownNetworks) )
				$knownNetworks[] = $net;
		}
		return $knownNetworks;
	}

	public function save($SSID, $password=null){}

	public function update($SSID,$password=null){}

	public function delete($SSID){}
	/**
	 * Runs a wireless network scan and sets its results
	 * to the networkList.
	 * @return $this
	 */
	public function scan(){
		$this->setNetworkList(shell_exec($this->scanNetworks));
		return $this;
	}
	/**
	 * Sets the network list according to command output.
	 * @param string $output Shell output from terminal commands.
	 */
	protected function setNetworkList($output) {
		$this->networkList = array(); // Clear existing list
		$rows = explode("\n", $output);
		foreach ($rows as $row) {
			$row = explode("\t", $row);
			if ($row[0])
				$this->addNet($this->makeNet($row[0], $row[3], $row[1]));
		}
	}
	/**
	 * Adds a network array to networkList
	 * @param array $net An array containing id, BSSID, ESSID.
	 */
	protected function addNet($net) {
		if ( $this->allowedBSSID($net["ESSID"]) )
			$this->networkList[] = $net;	
	}
	/**
	 * Makes a network array.
	 * @param  string $id    The id of the network result.
	 * @param  string $ESSID The network's ESSID.
	 * @param  string $BSSID The network's BSSID.
	 * @return array        A network array.
	 */
	protected function makeNet($id, $ESSID, $BSSID) {
		$net = array(
			"id" => $id,
			"ESSID" => $ESSID,
			"BSSID" => $BSSID
		);
		return $net;
	}
	/**
	 * Checks $id against disallowed BSSIDs and returns 
	 * true if it wasn't found.
	 * @param  string $BSSID The BSSID to test.
	 * @return boolean        True if the BSSID is allowed.
	 */
	protected function allowedBSSID($BSSID) {
		if ( ! $this->disallowedBSSIDs ) return true;
		if ( ! in_array($BSSID, $this->disallowedBSSIDs) ) return true;
	}


} // end Class

$n = new Networks;
print_r($n->scan()->listKnown());
// print_r($n->listNetworks());
?>