<?php
namespace TeamRadHq\Script\Traits;

/**
 * FileTraits contains methods for performing file and 
 * directory operations. 
 */
trait FileTraits {
	/**
	 * Sets the current working directory for executing commands. 
	 * Defaults to current directory.
	 * @var string
	 */
	protected $cwd;

	/**
	 * Returns the current working directory.
	 * @return string The path of the current working directory.
	 */
	public function pwd() {
		return getcwd();
	}
	/**
	 * Returns $dir's directory listing.
	 * @return array An array containing the directory contents.
	 */
	public function dir($dir=null) {
		$this->setCwd($dir);
		$results =  scandir($this->cwd);
		$results = array_diff($results, ['.','..']);
		return $results;
	}
	public function setCwd($dir=null) {
		if ( ! $dir ) {
			$this->cwd = $this->pwd();
		} else {
			$this->cwd = $dir;
		}
		$this->cwd.="/";
		$this->cwd = str_replace('//','/',$this->cwd);
		return $this;
	}
	public function rm($filename, $dir=null) {
		$this->setCwd($dir);
		$this->center($this->cwd);
		if ($this->exists($filename, $this->cwd))
			return unlink($this->cwd.$filename);
		$this->error("'$filename' does not exist!" );
		return false;
	}
	public function write($filename, $string, $dir=null) {
		$this->setCwd($dir);
		return file_put_contents($this->cwd.$filename, $string."\n");
	}
	public function exists($filename, $dir=null) {
		$this->setCwd($dir);
		return file_exists($this->cwd.$filename);
	}
}
?>