<?php
namespace TeamRadHq\Script;
class Script {
	use Traits\ArgumentTraits;
	use Traits\FileTraits;
	use Traits\OutputTraits;
	public function __construct($allowedFlags=array(), $allowedOptions=array()) {
		echo "\n"; // Escape terminal command
		// Set flags and options if they were passed.
		if( ! empty($allowedFlags)) 	$this->setAllowedFlags($allowedFlags);
		if( ! empty($allowedOptions)) 	$this->setAllowedOptions($allowedOptions);
		// Set the script arguments.
		$this->setArguments();
		// Load the colours helper
		$this->loadColors();
	}
}
?>